/**
 * 
 */
package io.javabrains.springbootstarter.topic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author varun
 *
 */
@RestController
public class TopicController {
	
	/**
	 * 
	 */
	@Autowired
	TopicService topicService;

	@RequestMapping("/topics")
	public List<Topic> getAllTopics() {
		return this.topicService.getAllTopics();

	}
	
	@RequestMapping("/topics/{id}")
	public Topic getTopic(@PathVariable String id) {
		return this.topicService.getTopic(id);
	}
	
	@RequestMapping(method=org.springframework.web.bind.annotation.RequestMethod.POST, value="/topics")
	public void addTopic(@RequestBody Topic topic) {
		this.topicService.addTopic(topic);
	}

}

/**
 * 
 */
package io.javabrains.springbootstarter.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

/**
 * @author varun
 *
 */
@Service
public class TopicService {
	
	/**
	 * List of topics
	 */
	private List<Topic> topics = new ArrayList<Topic>(Arrays.asList(new Topic("spring", "Spring Framework", "Spring Framework description"),
			new Topic("java", "Core Java", "Core Java description"),
			new Topic("javascript", "Javascript", "Javascript description")));
	
	/**
	 * 
	 * @return list of all topics
	 */
	public List<Topic> getAllTopics() {
		return this.topics;
	}
	
	/**
	 * 
	 * @param id
	 * @return topic with id
	 */
	public Topic getTopic(String id) {
//		Topic retVal = null;
//		for (Topic topic : this.topics) {
//			if(topic.getId().equalsIgnoreCase(id)) {
//				retVal = topic;
//			}
//		}
		return this.topics.stream().filter(t -> t.getId().equalsIgnoreCase(id)).findFirst().get();
		
	}

	/**
	 * 
	 * @param topic topic to add to list
	 */
	public void addTopic(Topic topic) {
		this.topics.add(topic);
		
	}

}
